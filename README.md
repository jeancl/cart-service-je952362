Docker DB setup for local env usage:

docker run --name local_mysql -e MYSQL_USER=admin -e MYSQL_PASSWORD=admin123 -e MYSQL_ALLOW_EMPTY_PASSWORD=true -e MYSQL_DATABASE=test -p 3306:3306 -d mysql

docker exec -it local_mysql bash

ALTER USER 'admin' IDENTIFIED WITH mysql_native_password BY 'admin123';