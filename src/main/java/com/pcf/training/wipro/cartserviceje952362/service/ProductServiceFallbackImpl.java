package com.pcf.training.wipro.cartserviceje952362.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.pcf.training.wipro.cartserviceje952362.domain.Product;

@Component
public class ProductServiceFallbackImpl implements ProductService {

    @Override
    public ResponseEntity<Product> getProduct(Long id) {
        return new ResponseEntity<>(null, HttpStatus.SERVICE_UNAVAILABLE);
    }

}
