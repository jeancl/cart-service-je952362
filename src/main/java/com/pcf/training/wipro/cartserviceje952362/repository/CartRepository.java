package com.pcf.training.wipro.cartserviceje952362.repository;

import org.springframework.data.repository.CrudRepository;

import com.pcf.training.wipro.cartserviceje952362.domain.Cart;

public interface CartRepository extends CrudRepository<Cart, Long> {

}
