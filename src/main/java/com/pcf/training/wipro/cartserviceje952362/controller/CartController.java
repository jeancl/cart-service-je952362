package com.pcf.training.wipro.cartserviceje952362.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pcf.training.wipro.cartserviceje952362.domain.Cart;
import com.pcf.training.wipro.cartserviceje952362.domain.Product;
import com.pcf.training.wipro.cartserviceje952362.repository.CartRepository;
import com.pcf.training.wipro.cartserviceje952362.service.ProductService;

@RestController
@RequestMapping(value = "/carts")
public class CartController {
    
    @Autowired
    private CartRepository cartRepo;
    
    @Autowired
    private ProductService productService;

    @PostMapping()
    public ResponseEntity<Cart> createCart() {
        return new ResponseEntity<>(cartRepo.save(new Cart()), HttpStatus.OK);
    }
    
    @GetMapping(value = "/{id}")
    public ResponseEntity<Cart> retreiveCartDetails(@PathVariable(value = "id") Long id) {
        
        Optional<Cart> optionalCart = cartRepo.findById(id);
        
        if(!optionalCart.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        return new ResponseEntity<>(optionalCart.get(), HttpStatus.OK);
    }
    
    @PostMapping(value = "/{id}/products")
    public ResponseEntity<Cart> addProduct(@PathVariable(value = "id") Long id, @RequestBody Product prod) {
        
        Optional<Cart> optionalCart = cartRepo.findById(id);
        
        if(!optionalCart.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        Cart cart = optionalCart.get();
        cart.setLastUpdatedDate(null);
        List<Product> productList = cart.getProducts();
        
        if(productList == null) {
            productList = new ArrayList<>();
        }
        
        ResponseEntity<Product> productEntity = productService.getProduct(prod.getId());
        
        if(productEntity.getStatusCode().equals(HttpStatus.NOT_FOUND) || productEntity.getBody() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if (productEntity.getStatusCode().equals(HttpStatus.SERVICE_UNAVAILABLE)) {
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }

        productList.add(productEntity.getBody());
        cart.setProducts(productList);
        
        return new ResponseEntity<>(cartRepo.save(cart), HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> deleteCartDetails(@PathVariable(value = "id") Long id) {
        cartRepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
