package com.pcf.training.wipro.cartserviceje952362.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pcf.training.wipro.cartserviceje952362.domain.Product;


@FeignClient(name="productService", url="https://product-service.apps.dev.pcf-aws.com", fallback=ProductServiceFallbackImpl.class)
public interface ProductService {

    @RequestMapping(value="/products/{id}", method=RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable(value = "id") Long id);
}
